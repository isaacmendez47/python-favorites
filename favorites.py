# Welcome to the second Git exercise.
# We're in a Python file this time, but the rules are
# mostly the same.

# Let's see what's different:
first_question = "What is your favorite color?"
# In Python, values inside quotation marks like you see above
# are called Strings. Strings can contain any combination of
# letters or numbers, so they're the perfect place to write our
# answers. Go ahead and write your answer to the first question
# in the string below:
first_question_answer = "My favorite color is grey"

# After you've committed and pushed, repeat the same process for
# the rest of the questions!

second_question = "What is your favorite food?"
second_question_answer = "My favorite food is pho with a bahn mi to the side"

third_question = "Who is your favorite fictional character? ex. Mickey Mouse, Bugs Bunny"
third_question_answer = "My favorite fictional character is Eren Yeager"

"""
!! Reminder that you should be:
  - adding your changes after you make them
  - committing the changes you added
  - pushing your commits to your forked repository
  After answering each question!!
"""

fourth_question = "What is your favorite animal?"
fourth_question_answer = "My favorite animal is my cat mocha, even though he doesnt like me much"

fifth_question = "What is your favorite programming language? (Hint: You can always say Python!!)"
fifth_question_answer = "My favorite programming language is python because i like snakes"
